///////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab02c - Cat `n Mouse - EE 205 - Spr 2022
///
/// This is a classic "I'm thinking of a number" guessing game.  The mouse
/// will think of a number... and the cat will keep trying to guess it.
///
/// @file    catNmouse.c
/// @version 1.0 - Initial version
///
/// Compile: $ gcc -o catNmouse catNmouse.c
///
/// Usage:  catNmouse [n]
///   n:  The maximum number used in the guessing game
///
/// Exit Status:
///   1:  The command line parameter was not a valid value
///   0:  The cat finally guessed correctly
///
/// Example:
///   $ ./catNmouse 2000
///   OK cat, I'm thinking of a number from 1 to 2000.  Make a guess:  1385
///   No cat... the number I’m thinking of is smaller than 1385
///   OK cat, I'm thinking of a number from 1 to 2000.  Make a guess:  1382
///   No cat... the number I’m thinking of is larger than 1382
///   OK cat, I'm thinking of a number from 1 to 2000.  Make a guess:  1384
///   You got me.
///   |\---/|
///   | o_o |
///    \_^_/
///   
/// @author  Adrian Peng <apeng2@hawaii.edu>
/// @date    21_Jan_2022
///////////////////////////////////////////////////////////////////////////////


#include <stdio.h>
#include <stdlib.h>

int main( int argc, char* argv[] ) {

   // Initialize some variables
   int default_max_number = 2048;
   int theMaxValue;
   int user_max_value;

   // Check if there is a parameter
   // If no,, then argc = 1; Set the_max_value = default_max_number
   // If yes, then argc = 2; Set check if the number is valid, then set the_max_value = user_max_value
   if (argc = 1)
   {
      theMaxValue = default_max_number;
   }   

   if (argc = 2)
   {
      int user_max_value = atoi(argv[1]);
      if (user_max_value < 1)
      {
         printf("Invalid number. Please input a number >= 1.\n");
         return 1;
      }
      else
      {
         theMaxValue = user_max_value;
      }
   }

   //Generate random integer from 1 to the_max_value
   int theNumberImThinkingOf;
   theNumberImThinkingOf = (rand() % (theMaxValue + 1));
   

   //The game
   int aGuess;

  do
  {
   printf("OK cat, I'm thinking of a number from 1 to %d.   Make a guess: ", theMaxValue);
   scanf("%d", &aGuess);
   
   if (aGuess < 1)
   {
      printf("You must enter a number that's >= 1\n");
   }

   if (aGuess > theMaxValue)
   {
      printf("You must enter a number that's <= %d\n", theMaxValue);
   }
   
   if (aGuess > theNumberImThinkingOf && aGuess <= theMaxValue)
   {
      printf("No cat... the number I'm thinking of is smaller than %d\n", aGuess);
   }

   if (aGuess < theNumberImThinkingOf && aGuess >= 1)
   {
      printf("No cat... the number I'm thinking of is larger than %d\n", aGuess);
   }

  } while (aGuess != theNumberImThinkingOf);
   
   printf("You got me.\n");
   printf("  |)---(|  \n");
   printf("  ( o o )  \n");
   printf("  ==_Y_==  bruh \n");
   printf("    `-'    \n");

   return 0;
}

